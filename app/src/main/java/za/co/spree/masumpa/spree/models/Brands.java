package za.co.spree.masumpa.spree.models;

import org.parceler.Parcel;

@Parcel
public class Brands {
    public String id;
    public String name;
    public String app_logo;
    public String _count;
}
