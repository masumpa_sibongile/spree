package za.co.spree.masumpa.spree.presenters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import java.util.List;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.co.spree.masumpa.spree.models.Brands;
import za.co.spree.masumpa.spree.rest.BrandService;
import za.co.spree.masumpa.spree.rest.Client;
import za.co.spree.masumpa.spree.views.ItemListActivity;

public class ItemsListPresenter {
    private ItemListActivity mView;
    private RecyclerView mRecyclerView;

    public ItemsListPresenter(ItemListActivity activity, @NonNull RecyclerView recyclerView) {
        mView = activity;
        mRecyclerView = recyclerView;
    }

    public void loadBrands() {
        BrandService getBrandServiceList = Client.getClient().create(BrandService.class);
        Observable<List<Brands>> brands = getBrandServiceList.getBrands();

        brands.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<List<Brands>>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    e.printStackTrace();
                }

                @Override
                public void onNext(List<Brands> brands) {
                    mView.displayBrands(mRecyclerView, brands);
                }
            });
    }
}

