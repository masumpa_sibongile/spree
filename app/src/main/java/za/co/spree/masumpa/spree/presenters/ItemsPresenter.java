package za.co.spree.masumpa.spree.presenters;


import android.support.annotation.NonNull;
import android.widget.ListView;
import java.util.List;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.co.spree.masumpa.spree.models.Product;
import za.co.spree.masumpa.spree.models.Products;
import za.co.spree.masumpa.spree.rest.BrandService;
import za.co.spree.masumpa.spree.rest.Client;
import za.co.spree.masumpa.spree.views.ItemDetailFragment;

public class ItemsPresenter {

    ItemDetailFragment mView;
    ListView mListView;

    public ItemsPresenter(ItemDetailFragment activity, @NonNull ListView listView) {
        mView = activity;
        mListView = listView;
    }

    public void loadProductsByBrandId(String brandId) {
        BrandService getBrandServiceList = Client.getClient().create(BrandService.class);
        Observable<Product> product = getBrandServiceList.getProductsByBrandId(brandId);

        product.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Product>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Product product) {
                        mView.displayProducts(mListView, product.products);
                    }
                });
    }

}
