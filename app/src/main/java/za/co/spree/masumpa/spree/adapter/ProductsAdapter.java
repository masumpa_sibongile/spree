package za.co.spree.masumpa.spree.adapter;
import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import za.co.spree.masumpa.spree.R;
import za.co.spree.masumpa.spree.models.Products;

public class ProductsAdapter extends BaseAdapter{

    public List<Products> data;
    public Context context;

    public ProductsAdapter(Context context, List<Products> items) {
        data = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Products product = (Products) getItem(position);
        ViewHolder holder = new ViewHolder();

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_detail_content, parent, false);
            holder.titleTv = (TextView) view.findViewById(R.id.item_title);
            holder.detailsTv = (TextView) view.findViewById(R.id.item_details);
            holder.whybuyTv = (TextView) view.findViewById(R.id.item_buy);


            if (product.title != null) {
                holder.titleTv.setText(product.title);
            }

            if (product.detail != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holder.detailsTv.setText(Html.fromHtml(product.detail,
                            Html.TO_HTML_PARAGRAPH_LINES_CONSECUTIVE));
                } else {
                    holder.detailsTv.setText(Html.fromHtml(product.detail));
                }
            }

            if (product.why_buy != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holder.whybuyTv.setText(Html.fromHtml(product.why_buy,
                            Html.TO_HTML_PARAGRAPH_LINES_CONSECUTIVE));
                } else {
                    holder.whybuyTv.setText(Html.fromHtml(product.why_buy));
                }
            }
        }

        return view;
    }

    static class ViewHolder {
        TextView titleTv, detailsTv, whybuyTv;
    }
}
