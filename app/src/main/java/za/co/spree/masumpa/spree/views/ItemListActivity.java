package za.co.spree.masumpa.spree.views;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import org.parceler.Parcels;
import za.co.spree.masumpa.spree.R;
import za.co.spree.masumpa.spree.helpers.Utils;
import za.co.spree.masumpa.spree.models.Brands;
import za.co.spree.masumpa.spree.presenters.ItemsListPresenter;
import java.util.ArrayList;
import java.util.List;

public class ItemListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    private static final String TAG = ItemListActivity.class.getSimpleName();

    private boolean mTwoPane;
    ItemsListPresenter mItemsListPresenter;
    BroadcastReceiver receiver;
    SimpleItemRecyclerViewAdapter simpleItemRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        final View recyclerView = findViewById(R.id.item_list);
        assert recyclerView != null;

        mItemsListPresenter = new ItemsListPresenter(this,(RecyclerView) recyclerView);

        if (findViewById(R.id.item_detail_container) != null) {
            mTwoPane = true;
        }

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Utils.isNetworkAvailable(intent, "connection")) {
                    mItemsListPresenter.loadBrands();
                }else{
                    Toast.makeText(getApplicationContext(), "No Internet Connection!", Toast.LENGTH_LONG).show();
                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver,
                new IntentFilter(
                        ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public void displayBrands(@NonNull RecyclerView recyclerView,List<Brands> brands){
        simpleItemRecyclerViewAdapter = new SimpleItemRecyclerViewAdapter(brands);
        recyclerView.setAdapter(simpleItemRecyclerViewAdapter);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        simpleItemRecyclerViewAdapter.getFilter().filter(newText);
        return true;
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> implements Filterable {

        private List<Brands> mValues;
        private List<Brands> filterdBrands;
        private BrandsFilter brandsFilter;

        SimpleItemRecyclerViewAdapter(List<Brands> items) {
            mValues = items;
            filterdBrands = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);

            if(holder.mItem.name != null) {
                    Picasso.Builder builder = new Picasso.Builder(getApplicationContext());
                    builder.listener(new Picasso.Listener() {
                        @Override
                        public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                            exception.printStackTrace();
                            Log.d(TAG, "URI: " + uri.toString());
                        }
                    });
                    builder.build().load(Utils.getImageUrl(holder.mItem.app_logo))
                            .error(R.drawable.avatar_placeholder)
                            .placeholder(R.drawable.avatar_placeholder)
                            .noFade()
                            .into(holder.mBrandImage);
                    holder.mTitleView.setText(holder.mItem.name);
            }

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putParcelable(ItemDetailFragment.ARG_ITEM_ID, Parcels.wrap(holder.mItem));
                        ItemDetailFragment fragment = new ItemDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.item_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, ItemDetailActivity.class);
                        intent.putExtra(ItemDetailFragment.ARG_ITEM_ID, Parcels.wrap(holder.mItem));

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final View mView;
            final TextView mTitleView;
            final ImageView mBrandImage;
            Brands mItem;

            ViewHolder(View view) {
                super(view);
                mView = view;
                mTitleView = (TextView) view.findViewById(R.id.title);
                mBrandImage = (ImageView) view.findViewById(R.id.brandImage);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mTitleView.getText() + "'";
            }
        }

        @Override
        public Filter getFilter() {
            if (brandsFilter == null) {
                brandsFilter = new BrandsFilter();
            }

            return brandsFilter;
        }

        private class BrandsFilter extends Filter{

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint!=null && constraint.length()>0) {
                    ArrayList<Brands> tempList = new ArrayList<Brands>();

                    // search content in friend list
                    for (Brands brands : filterdBrands) {
                        if (brands.name.toLowerCase().contains(constraint.toString().toLowerCase())) {
                            tempList.add(brands);
                        }
                    }

                    filterResults.count = tempList.size();
                    filterResults.values = tempList;
                } else {
                    filterResults.count = filterdBrands.size();
                    filterResults.values = filterdBrands;
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                SimpleItemRecyclerViewAdapter.this.mValues = (ArrayList<Brands>) filterResults.values;
                notifyDataSetChanged();
            }
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(ItemListActivity.this);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onStop(){
        super.onStop();
        unregisterReceiver(receiver);
    }

}
