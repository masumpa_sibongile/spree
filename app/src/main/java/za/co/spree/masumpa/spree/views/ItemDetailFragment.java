package za.co.spree.masumpa.spree.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.List;

import za.co.spree.masumpa.spree.R;
import za.co.spree.masumpa.spree.adapter.ProductsAdapter;
import za.co.spree.masumpa.spree.helpers.Utils;
import za.co.spree.masumpa.spree.models.Brands;
import za.co.spree.masumpa.spree.models.Product;
import za.co.spree.masumpa.spree.models.Products;
import za.co.spree.masumpa.spree.presenters.ItemsListPresenter;
import za.co.spree.masumpa.spree.presenters.ItemsPresenter;

public class ItemDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item_id";
    private Brands mItem;
    ItemsPresenter mItemsPresenter;
    Toolbar appBarLayout;

    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ItemDetailFragment.ARG_ITEM_ID)) {
            mItem = Parcels.unwrap(getArguments().getParcelable(ItemDetailFragment.ARG_ITEM_ID));

            Activity activity = this.getActivity();
            appBarLayout = (Toolbar) activity.findViewById(R.id.detail_toolbar);
            if (appBarLayout != null) {
                appBarLayout.setTitleTextColor(Color.WHITE);
                appBarLayout.setTitle(mItem.name);
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);



        View listView = rootView.findViewById(R.id.item_detail_recycler);
        assert listView != null;

        mItemsPresenter = new ItemsPresenter(ItemDetailFragment.this,(ListView) listView);
        mItemsPresenter.loadProductsByBrandId(mItem.id);

        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.item_brand_name)).setText(mItem.name);
            ((TextView) rootView.findViewById(R.id.available_items)).setText("Available items: " + mItem._count);
        }

        return rootView;
    }

    public void displayProducts(@NonNull ListView listView, List<Products> products){
        listView.setAdapter(new ProductsAdapter(getActivity(),products));
    }


}
