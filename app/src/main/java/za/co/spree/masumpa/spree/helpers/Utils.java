package za.co.spree.masumpa.spree.helpers;

import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Bundle;

/**
 * Created by Masumpa on 2016/10/09.
 */

public class Utils {
    private static NetworkInfo activeNetwork;
    public static final String MEDIA_BASE_URL = "https://api-dev.spreeza.net/media/";

    public static String getImageUrl(String imageEndpoint){
        if(imageEndpoint!=null){
            imageEndpoint = MEDIA_BASE_URL + imageEndpoint;
        }
        return imageEndpoint;
    }

    public static boolean isNetworkAvailable(Intent intent, String tag){
        Bundle extras = intent.getExtras();
        activeNetwork = (NetworkInfo)extras.get("networkInfo");
        if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){return true;}else{return false;}
    }
}
