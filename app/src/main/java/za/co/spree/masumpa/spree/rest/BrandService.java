package za.co.spree.masumpa.spree.rest;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;
import za.co.spree.masumpa.spree.models.Brands;
import za.co.spree.masumpa.spree.models.Product;
import za.co.spree.masumpa.spree.models.Products;

/**
 * Created by Masumpa on 2016/10/07.
 */

public interface BrandService{
    @GET("catalog/brands")
    Observable<List<Brands>> getBrands();

    @GET("catalog/brand/{brand_id}")
    Observable<Product> getProductsByBrandId(@Path("brand_id") String brandId);

}
