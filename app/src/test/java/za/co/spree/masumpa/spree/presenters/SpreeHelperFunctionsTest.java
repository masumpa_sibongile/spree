package za.co.spree.masumpa.spree.presenters;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.co.spree.masumpa.spree.helpers.Utils;
import za.co.spree.masumpa.spree.models.Brands;
import za.co.spree.masumpa.spree.rest.BrandService;
import za.co.spree.masumpa.spree.rest.Client;

@RunWith(JUnit4.class)
public class SpreeHelperFunctionsTest extends TestCase {

    @Test
    public void mediaUrlTest(){
        String url = "https://api-dev.spreeza.net/media/xyd/chucknorris.jpg";
        assertEquals(url,Utils.getImageUrl("xyd/chucknorris.jpg"));
    }

    @Test
    public void getBrandsTest(){
        BrandService getBrandServiceList = Client.getClient().create(BrandService.class);
        Observable<List<Brands>> brands = getBrandServiceList.getBrands();

        brands.subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .subscribe(new Observer<List<Brands>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(List<Brands> brands) {
                        assertTrue("Size greater than zero", brands.size() > 0);
                        assertNotNull(brands);

                    }
                });
    }

}