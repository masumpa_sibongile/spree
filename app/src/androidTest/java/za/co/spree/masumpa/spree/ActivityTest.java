package za.co.spree.masumpa.spree;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import za.co.spree.masumpa.spree.views.ItemListActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class ActivityTest {

    @Rule
    public final ActivityTestRule<ItemListActivity> main = new ActivityTestRule<>(ItemListActivity.class);

    @Test
    public void launchMainScreen(){
        onView(withText("Spree")).check(ViewAssertions.matches(isDisplayed()));
        onView(withId(R.id.frameLayout)).check(ViewAssertions.matches(isDisplayed()));
    }
}
