# Set Up #

Clone repository to your local machine, "PLEASE CONTACT ME ON MIGHTYMASUMPA@GMAIL.COM SO THAT I CAN SHARE THE PROJECT WITH YOU FROM BITBUCKET"

Build project, and run project.

_________________________________________________

**Libraries used**

compile 'com.squareup.picasso:picasso:2.5.2'
compile 'com.squareup.okhttp:okhttp:2.5.0'
    
compile 'io.reactivex:rxjava:1.0.4'
compile 'io.reactivex:rxandroid:0.24.0'

compile 'com.squareup.retrofit2:retrofit:2.1.0'
compile 'com.squareup.retrofit2:converter-gson:2.1.0'
compile 'com.squareup.retrofit2:adapter-rxjava:2.1.0'

compile "org.parceler:parceler-api:1.0.3"
apt "org.parceler:parceler:1.0.3"

androidTestCompile 'com.android.support.test.espresso:espresso-core:2.2.2'

______________________________________________________________________________


I tried to keep the App as simple as possible, 
The search can only search downloaded brands,

app_deburg.apk is 2.4MB
app_realease.apk is 1.4MB.


________________________________________________________________________________
**
Very basic testing.**

I have included two unit tests inside class SpreeHelperFunctionsTest.java. you can right click each function and Test.

I have also included one instrumental test inside class ActivityTest.java. you can right click the function and test.

________________________________________________________________________________

I made use of MVP, RxJava, and build using gradle.
The app is straight forward and covers all requirements specified.

Thanks